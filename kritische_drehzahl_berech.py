import math

# Länge der Welle in m

l = 0.75

# Abstand der Scheiben in m (Differenz der Lagerungen)

s = 0.5 - 0.25

# E-Modul der Welle in N/m**2

E = 210000*10**6

# Durchmesser der Welle in m

d = 0.008

# Masse einer Scheibe in kg

m = 0.5

# y

y = s/l

# a_1 und a_2

a_1 = 1 - 2*y*y + y*y*y*y
a_2 = 1 - 4*y*y + 4*y*y*y - y*y*y*y

# Flächenträgheitsmoment berechnen

I = math.pi*d*d*d*d/64

# Ersatzfedersteifigkeit k berechnen in N/m

k = 48*E*I/(l*l*l)

# Kritische Kreisfrequenzen in U/min

w_1 = math.sqrt(k/(m*(a_1 + a_2)))*60/(2*math.pi)
w_2 = math.sqrt(k/(m*(a_1 - a_2)))*60/(2*math.pi)

print(w_1)
print(w_2)