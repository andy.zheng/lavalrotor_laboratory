# %% Functions for processing data
import os
import json
import numpy as np
from numpy.fft import rfft, fft
from typing import Dict, Tuple


def get_my_data_from_dataset(data_path, sensor_ID, motor_ID):
    import h5py
    hdf5_file = h5py.File(data_path, "r")
    
    a = 0
    b = 1
    data_list =  []
    while b == 1 :
        try:
            ##motor_rpm
            # Access the datasets for motor_rpm
            motor_rpm = hdf5_file[str(a)+ "/RawData/" + motor_ID +"/motor_rpm"]
            # Convert the motor_rpm datasets to NumPy arrays
            motor_rpm_array = np.array(motor_rpm)
            
            ##sensor_acceleration_x
            # Access the datasets for sensor_acceleration_x
            sensor_acceleration_x = hdf5_file[str(a)+ "/RawData/" + sensor_ID +"/acceleration_x"]
            # Convert the acceleration_x datasets to NumPy arrays
            sensor_acceleration_x_array = np.array(sensor_acceleration_x)

            ##sensor_acceleration_y
            # Access the datasets for sensor_acceleration_y
            sensor_acceleration_y = hdf5_file[str(a)+ "/RawData/" + sensor_ID +"/acceleration_y"]
            # Convert the acceleration_y datasets to NumPy arrays
            sensor_acceleration_y_array = np.array(sensor_acceleration_y)

            ##sensor_acceleration_z
            # Access the datasets for sensor_acceleration_z
            sensor_acceleration_z = hdf5_file[str(a)+ "/RawData/" + sensor_ID +"/acceleration_z"]
            # Convert the acceleration_z datasets to NumPy arrays
            sensor_acceleration_z_array = np.array(sensor_acceleration_z)

            ##sensor_timestamp
            # Access the datasets for sensor_timestamp
            sensor_timestamp = hdf5_file[str(a)+ "/RawData/" + sensor_ID +"/timestamp"]
            # Convert the timestamp datasets to NumPy arrays
            sensor_timestamp_array = np.array(sensor_timestamp)

            data_list.append([sensor_acceleration_x_array, sensor_acceleration_y_array, sensor_acceleration_z_array, sensor_timestamp_array ,motor_rpm_array])

            
            a += 1
            
        except:
            b = 0
            return data_list
            
            
            
    
def get_my_interpolation_points(data_list) : 
    
    ##data_list analysieren und größte-ErsteMessung-Zeit, kleinste-LetzteMessung-Zeit, kleinsteAnzahl-Messpunkten ermitteln
    max_start_time = data_list[0][3][0]
    min_end_time = data_list[0][3][-1]
    min_mess_count = len(data_list[0][3])
    
    for i in range(len(data_list)):
        if max_start_time < data_list[i][3][0] :
            max_start_time = data_list[i][3][0]
        if min_end_time > data_list[i][3][-1] :
            min_end_time = data_list[i][3][-1]
        if min_mess_count > len(data_list[i][3]) :
            min_mess_count = len(data_list[i][3])
    
    #Anzahl der Interpol Zeitpunkte anpassen
    #min_mess_count = 2*min_mess_count
    
    
    
    #interpolierte timestamps
    inter_timestamps = np.linspace(max_start_time, min_end_time, min_mess_count, endpoint=True)

    return inter_timestamps
    
    
    


def get_vec_accel(x: np.ndarray, y: np.ndarray, z: np.ndarray) -> np.ndarray:
    """Calculates the vector absolute value of the temporal evolution of a vector (x, y, z).

    Args:
        x (ndarray): Vector containing the temporal elements in the first axis direction.
        y (ndarray): Vector containing the temporal elements in the second axis direction.
        z (ndarray): Vector containing the temporal elements in the third axis direction.

    Returns:
        (ndarray): Absolute value of the evolution.
    """
    #berechnung des Betrages eines jeden gemessenen Beschleunigungsvektor
    besch_betr = np.sqrt(x*x + y*y + z*z)
    
    return besch_betr

def interpolation(time: np.ndarray, data: np.ndarray, int_time: np.ndarray) -> np.ndarray:
    """Linearly interpolates values in data.

    Args:
        time (ndarray): Timestamp of the values in data
        data (ndarray): Values to interpolate
        int_time (ndarray): Points in time at which the data is to be interpolated.

    Returns:
        int_data (ndarray): Interpolation points based on 'time'.
    """
    
    #lineare interpolation
    int_data = np.interp(int_time, time, data)
    
    
    return np.array(int_data)          
    
   


def my_fft_scaled(x: np.ndarray, time: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Calculates the FFT of x (with numpy fft() or rfft()) and scales the FFT amplitude.

    It is assumed that the time interval between the sampled sensor data
    is constant.

    Args:
        x (ndarray): Measurement data that are transformed into the
            frequency domain.
        time (ndarray): Timestamp of the measurement data

    Returns:
        (ndarray): Scaled Amplitude of the computed FFT spectrum
        (ndarray): Frequency of the computed FFT spectrum
    """
    
    
    #rfft
    signal = rfft(x - np.mean(x))
    
    #frequenz
    n = x.size
    sample_rate = 1 /(time[2] - time[1])
    freq = np.fft.rfftfreq(n, d=1./sample_rate)
    #rfft amplitude scaling
    signal_scaled =  (2.0 / n ) * signal
    
    
    return [signal_scaled, freq]
    
    
    


def evaluate_measurement_metadata(folder_path_metadata: str) -> Dict:
    """Finds the path to the JSON setup file and generates the setup_dict from it.

    Args:
        folder_path_metadata (str): Path to the folder with all relevant JSON metadata files, including JSON setup file.

    Returns:
        dict: Information in the setup_json_path file, enriched with the paths to the JSON files of the components.
    """
    for dir_path, _, file_names in os.walk(folder_path_metadata):
        for file_name in file_names:
            if file_name.endswith((".json")):
                file_path = os.path.join(dir_path, file_name)
                with open(file_path, "r") as json_file:
                    json_content = json.load(json_file)
                    if "setup" in json_content.keys():
                        setup_json_path = file_path
    return evaluate_setup(setup_json_path, folder_path_metadata)


def evaluate_setup(setup_json_path: str, folder_path_metadata: str) -> Dict:
    """Scans all metadata specified in the file with the path equal to setup_json_path.

    Args:
        setup_json_path (str): Path to setup JSON file.
        folder_path_metadata (str): Path where the JSON files of the components are located.

    Raises:
        RuntimeError: Is triggered if the information in the component JSON file does not match the information
            in the JSON-Setup file.

    Returns:
        dict: Information in the setup_json_path file, enriched with the paths to the JSON files of the components.
    """
    with open(setup_json_path, "r") as json_file:
        json_content = json.load(json_file)

    setup_dict = json_content["setup"]

    for dir_path, _, file_names in os.walk(folder_path_metadata):
        for file_name in file_names:
            if file_name.endswith((".json")):
                file_path = os.path.join(dir_path, file_name)
                with open(file_path, "r") as json_file:
                    json_content = json.load(json_file)
                    json_file_uuid = json_content["JSON"]["ID"]
                if json_file_uuid in setup_dict.keys():
                    json_file_type = list(json_content.keys())[1]
                    if setup_dict[json_file_uuid]["type"] == json_file_type:
                        setup_dict[json_file_uuid]["path"] = file_path
                    else:
                        print(
                            "Something is wrong with {} or the corresponding entry in the JSON setup file.".format(
                                file_path
                            )
                        )
                        raise RuntimeError(
                            "Metadata in JSON files is not specified correctly."
                        )

    return setup_dict


def extract_uuid(setup_dict: Dict) -> Tuple[str, str]:
    """Extratcts uuid of measuring devices from setup_dict.

    Args:
        setup_dict (dict): Information in the setup_json_path file.

    Returns:
        id_accelerometer (str): Uuid of 'accelerometer', required to read the acceleration from the h5 file
        id_hall (str):  Uuid of 'motor_controller', required to read the motor-rpm from the h5 file.
    """
    id_accelerometer = None
    id_hall = None
    for component_dict, component_uuid in zip(setup_dict.values(), setup_dict.keys()):
        if "sensor" in component_dict["type"]:
            if component_dict["name"] == "accelerometer":
                if id_accelerometer is None:
                    id_accelerometer = component_uuid
                else:
                    print("Several 'acccelerometers' defined in the JSON setup file")

        elif "instrument" in component_dict["type"]:
            if component_dict["name"] == "motor_controller":
                if id_hall is None:
                    id_hall = component_uuid
                else:
                    print("Several 'motor_controller' defined in the JSON setup file")

    if (id_accelerometer is None) or (id_hall is None):
        raise RuntimeError(
            "Relevant information is missing in the JSON setup file."
        )
    return id_accelerometer, id_hall
